﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
	Inherits System.Windows.Forms.Form

	'Форма переопределяет dispose для очистки списка компонентов.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Является обязательной для конструктора форм Windows Forms
	Private components As System.ComponentModel.IContainer

	'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
	'Для ее изменения используйте конструктор форм Windows Form.  
	'Не изменяйте ее в редакторе исходного кода.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
		Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
		Me.CheckBox1 = New System.Windows.Forms.CheckBox()
		Me.Button1 = New System.Windows.Forms.Button()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.VirtKeyBtn1 = New Cf.VirtualKeyboards.VirtKeyBtn(Me.components)
		Me.StdKeyboard1 = New Cf.VirtualKeyboards.StdKeyboard(Me.components)
		Me.SuspendLayout()
		'
		'RichTextBox1
		'
		Me.RichTextBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.RichTextBox1.Location = New System.Drawing.Point(0, 12)
		Me.RichTextBox1.Name = "RichTextBox1"
		Me.RichTextBox1.Size = New System.Drawing.Size(634, 306)
		Me.RichTextBox1.TabIndex = 1
		Me.RichTextBox1.Text = ""
		'
		'CheckBox1
		'
		Me.CheckBox1.AutoSize = True
		Me.CheckBox1.Location = New System.Drawing.Point(496, 29)
		Me.CheckBox1.Name = "CheckBox1"
		Me.CheckBox1.Size = New System.Drawing.Size(90, 17)
		Me.CheckBox1.TabIndex = 2
		Me.CheckBox1.Text = "Ввод пароля"
		Me.CheckBox1.UseVisualStyleBackColor = True
		'
		'Button1
		'
		Me.Button1.Location = New System.Drawing.Point(399, 163)
		Me.Button1.Name = "Button1"
		Me.Button1.Size = New System.Drawing.Size(75, 23)
		Me.Button1.TabIndex = 3
		Me.Button1.Text = "Button1"
		Me.Button1.UseVisualStyleBackColor = False
		'
		'Label1
		'
		Me.Label1.BackColor = System.Drawing.SystemColors.ActiveCaption
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
		Me.Label1.Location = New System.Drawing.Point(448, 83)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(159, 59)
		Me.Label1.TabIndex = 4
		Me.Label1.Text = "Label1"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'VirtKeyBtn1
		'
		Me.VirtKeyBtn1.FocusControl = Me.RichTextBox1
		Me.VirtKeyBtn1.Image = CType(resources.GetObject("VirtKeyBtn1.Image"), System.Drawing.Image)
		Me.VirtKeyBtn1.Location = New System.Drawing.Point(346, 42)
		Me.VirtKeyBtn1.Name = "VirtKeyBtn1"
		Me.VirtKeyBtn1.PassMode = True
		Me.VirtKeyBtn1.Size = New System.Drawing.Size(79, 57)
		Me.VirtKeyBtn1.TabIndex = 5
		Me.VirtKeyBtn1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
		Me.VirtKeyBtn1.UseVisualStyleBackColor = True
		'
		'StdKeyboard1
		'
		Me.StdKeyboard1.BackColor = System.Drawing.Color.Firebrick
		Me.StdKeyboard1.BtnClickColor = System.Drawing.Color.White
		Me.StdKeyboard1.BtnColor = System.Drawing.Color.Silver
		Me.StdKeyboard1.BtnOnMouseHoverColor = System.Drawing.Color.Yellow
		Me.StdKeyboard1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.StdKeyboard1.FocusedControl = Nothing
		Me.StdKeyboard1.Location = New System.Drawing.Point(0, 324)
		Me.StdKeyboard1.Name = "StdKeyboard1"
		Me.StdKeyboard1.PasswordMode = False
		Me.StdKeyboard1.Size = New System.Drawing.Size(634, 209)
		Me.StdKeyboard1.TabIndex = 0
		'
		'Form1
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(634, 533)
		Me.Controls.Add(Me.VirtKeyBtn1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Button1)
		Me.Controls.Add(Me.CheckBox1)
		Me.Controls.Add(Me.RichTextBox1)
		Me.Controls.Add(Me.StdKeyboard1)
		Me.Name = "Form1"
		Me.Text = "Test"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents StdKeyboard1 As Cf.VirtualKeyboards.StdKeyboard
	Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
	Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
	Friend WithEvents Button1 As System.Windows.Forms.Button
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents VirtKeyBtn1 As Cf.VirtualKeyboards.VirtKeyBtn

End Class
