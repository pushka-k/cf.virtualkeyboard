﻿Imports System.Threading

Public Class Form1
	Public Sub New()
		InitializeComponent()
	End Sub

	Public Sub OnKeyPressed(key As String) Handles StdKeyboard1.KeyPressed
		RichTextBox1.Focus()
		SendKeys.Send(key)
	End Sub

	Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
		StdKeyboard1.PasswordMode = CheckBox1.Checked
	End Sub

	Private Sub Label1_MouseHover(sender As Object, e As EventArgs) Handles Label1.MouseHover, Label1.MouseEnter, Label1.MouseMove
		Label1.BackColor = Color.Gray
	End Sub

	Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
		Dim f = Sub()
					Dim c = Label1.BackColor
					Dim f1 = Sub()
								 Label1.BackColor = Color.White
							 End Sub
					Label1.Invoke(f1)
					Thread.Sleep(50)
					Dim f2 = Sub()
								 Label1.BackColor = c
							 End Sub
					Label1.Invoke(f2)
				End Sub
		Dim t = New Thread(f)
		t.Start()
	End Sub

	Private Sub Label1_MouseLeave(sender As Object, e As EventArgs) Handles Label1.MouseLeave
		Label1.BackColor = Color.Green
	End Sub
End Class
