﻿Imports System.Windows.Forms
Imports System.Drawing

Public Class VirtKeyBtn
	Inherits Button

	Public Property FocusControl As Control

	Public Property PassMode As Boolean

	Private Function GetCoords() As Point
		Dim x = Left
		Dim y = Top
		Dim parent = Me.Parent
		While parent IsNot Nothing
			x += parent.Left
			y += parent.Top
			parent = parent.Parent
		End While
		Return New Point(x, y)
	End Function

	Private Sub VirtKeyBtn_Click(sender As Object, e As EventArgs) Handles MyBase.Click
		Dim form = FindForm()
		Dim location = GetCoords()

		Dim x = location.X - 470
		If x < 50 Then
			x = 50
		End If
		Dim f = New VirtKeysForm(FocusControl, x, location.Y + 70, False)
		f.PassMode = PassMode
		f.Show(form)
	End Sub
End Class
