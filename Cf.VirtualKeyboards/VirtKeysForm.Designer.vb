﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VirtKeysForm
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VirtKeysForm))
		Me.VirtKeysForForm1 = New VirtualKeyboards.VirtKeys2()
		Me.SuspendLayout()
		'
		'VirtKeysForForm1
		'
		Me.VirtKeysForForm1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.VirtKeysForForm1.BackColor = System.Drawing.Color.DimGray
		Me.VirtKeysForForm1.BtnClickColor = System.Drawing.Color.White
		Me.VirtKeysForForm1.BtnColor = System.Drawing.SystemColors.Control
		Me.VirtKeysForForm1.BtnOnMouseHoverColor = System.Drawing.Color.Gainsboro
		Me.VirtKeysForForm1.FocusedControl = Nothing
		Me.VirtKeysForForm1.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
		Me.VirtKeysForForm1.Location = New System.Drawing.Point(12, 12)
		Me.VirtKeysForForm1.Name = "VirtKeysForForm1"
		Me.VirtKeysForForm1.PasswordMode = False
		Me.VirtKeysForForm1.Size = New System.Drawing.Size(954, 280)
		Me.VirtKeysForForm1.TabIndex = 0
		'
		'VirtKeysForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.AutoValidate = System.Windows.Forms.AutoValidate.Disable
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ClientSize = New System.Drawing.Size(978, 305)
		Me.Controls.Add(Me.VirtKeysForForm1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.MinimumSize = New System.Drawing.Size(735, 236)
		Me.Name = "VirtKeysForm"
		Me.ShowInTaskbar = False
		Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.Text = "Виртуальная клавиатура"
		Me.TopMost = True
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents VirtKeysForForm1 As VirtKeys2
End Class
