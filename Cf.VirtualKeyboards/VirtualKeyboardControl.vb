﻿Imports System.Windows.Forms
Imports System.Windows.Forms.VisualStyles
Imports System.Drawing
Imports System.Threading

Public Class VirtualKeyboardControl
	Inherits Panel

	Private _specChars As String = "+[](){}^%~"
	Private _specStrings As String() = {"enter", "home", "end", "del"}
	Private _changing As Boolean = False
	Private _otherLang As Boolean = False
	Protected _keys As IEnumerable(Of IEnumerable(Of KeyInfo))
	Private ReadOnly _locker As New Object
	Private _passwordMode
	Private _clickProcessed As Boolean = False
	Private _btnColor As Color
	Private _btnClickColor As Color
	Private _btnOnMouseHoverColor As Color

	Public Event KeyPressed(key As String)

	Public Sub New(keys As IEnumerable(Of IEnumerable(Of KeyInfo)))
		InitializeComponent()
		SetKeys(keys)
	End Sub

	
	Public Sub UpdateBtn()
		ShowKeysText()
	End Sub

	Protected Sub SetKeys(keys As IEnumerable(Of IEnumerable(Of KeyInfo)))
		SyncLock (_locker)
			If _keys IsNot Nothing Then
				For Each keyRow In _keys
					If keyRow IsNot Nothing Then
						For Each key In keyRow
							If key IsNot Nothing Then
								If key.Control IsNot Nothing Then
									Dim btn = CType(key.Control, Button)
									RemoveHandler btn.Click, AddressOf OnKeyPressed
									RemoveHandler btn.DoubleClick, AddressOf OnKeyPressed
									Me.Controls.Remove(btn)
								End If
							End If
						Next
					End If
				Next
			End If

			_keys = keys
			If _keys IsNot Nothing Then
				For Each keyRow In _keys
					If keyRow IsNot Nothing Then
						For Each key In keyRow
							If key IsNot Nothing Then
								Dim lab = New Label
								lab.AutoSize = False
								key.Control = lab
								lab.TextAlign = Drawing.ContentAlignment.MiddleCenter
								lab.Tag = key

								If key.Text.ToLower = "shift" Or key.Text.ToLower = "caps" Then
									key.LikeBtn = True
								End If

								AddHandler lab.Click, AddressOf OnKeyPressed
								AddHandler lab.DoubleClick, AddressOf OnKeyPressed

								AddHandler lab.MouseHover, AddressOf Lab_MouseHover
								AddHandler lab.MouseEnter, AddressOf Lab_MouseHover
								AddHandler lab.MouseMove, AddressOf Lab_MouseHover
								AddHandler lab.MouseLeave, AddressOf Lab_MouseLeave

								Me.Controls.Add(lab)
								lab.Parent = Me
							End If
						Next
					End If
				Next
			End If
		End SyncLock
		ShowKeysText()
		OnResize(Nothing)
	End Sub

	Protected Sub ShowKeysText()
		SyncLock (_locker)
			If _keys IsNot Nothing Then
				Dim shift = False
				For Each keyRow In _keys
					If keyRow IsNot Nothing Then
						For Each key In keyRow
							If key IsNot Nothing AndAlso key.LikeBtn AndAlso CType(key.Control, Label).BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
								If key.Text.ToLower = "shift" Then
									shift = True
								End If
							End If
						Next
					End If
				Next

				For Each keyRow In _keys
					If keyRow IsNot Nothing Then
						For Each key In keyRow
							If key IsNot Nothing Then
								If shift AndAlso key.LikeBtn Then
									key.Control.BackColor = BtnClickColor
								Else
									key.Control.BackColor = BtnColor
								End If

								If Not key.LangBtn Then
									If Not String.IsNullOrWhiteSpace(key.TextShifted) Then
										If key.OtherLanguage Then
											If _otherLang Then
												key.Control.Text = key.Text
											Else
												key.Control.Text = key.TextShifted
											End If

											If shift Then
												key.Control.Text = key.Control.Text.ToUpper
											End If
										Else
											If shift Then
												key.Control.Text = key.TextShifted
											Else
												key.Control.Text = key.Text
											End If
										End If
									Else
										key.Control.Text = key.Text
									End If
								Else
									If _otherLang Then
										key.Control.Text = key.Text
									Else
										key.Control.Text = key.TextShifted
									End If
								End If
							End If
						Next
					End If
				Next
			End If
		End SyncLock
	End Sub

	Public Property FocusedControl As Control

	Public Property BtnColor As Color
		Get
			Return _btnColor
		End Get
		Set(value As Color)
			_btnColor = value
			ShowKeysText()
		End Set
	End Property

	Public Property BtnClickColor As Color
		Get
			Return _btnClickColor
		End Get
		Set(value As Color)
			_btnClickColor = value
			ShowKeysText()
		End Set
	End Property

	Public Property BtnOnMouseHoverColor As Color
		Get
			Return _btnOnMouseHoverColor
		End Get
		Set(value As Color)
			_btnOnMouseHoverColor = value
			ShowKeysText()
		End Set
	End Property


	Public Property PasswordMode As Boolean
		Get
			Return _passwordMode
		End Get
		Set(value As Boolean)
			_passwordMode = value
			ShowKeysText()
		End Set
	End Property

	Private Sub OnKeyPressed(sender As Object, e As EventArgs)
		SyncLock (_locker)
			If Not _changing Then
				_changing = True

				If CType(CType(sender, Control).Tag, KeyInfo).LikeBtn Then
					Dim shift = False

					If sender.Text.ToLower = "shift" Then
						If CType(sender, Label).BorderStyle = Windows.Forms.BorderStyle.Fixed3D Then
							CType(sender, Label).BorderStyle = Windows.Forms.BorderStyle.None
							CType(sender, Label).BackColor = BtnColor
							shift = False
						Else
							CType(sender, Label).BorderStyle = Windows.Forms.BorderStyle.Fixed3D
							CType(sender, Label).BackColor = BtnClickColor
							shift = True
						End If
					End If

					For Each keyRow In _keys
						If keyRow IsNot Nothing Then
							For Each key In keyRow
								If key IsNot Nothing AndAlso key.LikeBtn Then
									If key.Text.ToLower = "shift" Then
										If Not shift Then
											CType(sender, Label).BorderStyle = Windows.Forms.BorderStyle.None
											CType(sender, Label).BackColor = BtnColor
										Else
											CType(sender, Label).BorderStyle = Windows.Forms.BorderStyle.Fixed3D
											CType(sender, Label).BackColor = BtnClickColor
										End If
									End If
								End If
							Next
						End If
					Next
					ShowKeysText()
				Else
					Dim label = CType(sender, Label)

					If Not PasswordMode Then
						Dim f = Sub()
									Try
										_clickProcessed = True
										Dim c = label.BackColor
										Dim f1 = Sub()
													 Try
														 label.BackColor = BtnClickColor
													 Catch ex As Exception
													 End Try
												 End Sub
										label.Invoke(f1)
										Thread.Sleep(100)
										Dim f2 = Sub()
													 Try
														 If label.BackColor <> BtnColor Then
															 label.BackColor = c

														 End If
														 _clickProcessed = False
													 Catch ex As Exception
													 End Try
												 End Sub
										label.Invoke(f2)
									Catch ex As Exception
									End Try
								End Sub
						Dim t = New Thread(f)
						t.Start()
					End If

					Dim key = CType(label.Tag, KeyInfo)
					If key.LangBtn Then
						_otherLang = Not _otherLang
						ShowKeysText()
					Else
						Dim keyStr = label.Text
						If _specStrings.Contains(keyStr.ToLower) Then
							keyStr = "{" + keyStr.ToUpper + "}"
						ElseIf keyStr.ToLower = "←" Then
							keyStr = "{BACKSPACE}"
						ElseIf keyStr.ToLower = "ввод" Then
							keyStr = "{ENTER}"
						ElseIf keyStr.ToLower = "пробел" Or keyStr.ToLower = "space" Then
							keyStr = " "
						ElseIf keyStr.ToLower = "tab" Then
							keyStr = "^{TAB}"
						ElseIf _specChars.Contains(keyStr.ToLower) Then
							keyStr = "{" + keyStr + "}"
						End If

						If FocusedControl IsNot Nothing Then
							FocusedControl.Focus()
							SendKeys.Send(keyStr)
						Else
							RaiseEvent KeyPressed(keyStr)
						End If
					End If
				End If
			End If
			_changing = False
		End SyncLock
	End Sub

	Private Sub Lab_MouseHover(sender As Object, e As EventArgs)
		If (Not _clickProcessed) And (Not PasswordMode) Then
			Dim ki = CType(CType(sender, Control).Tag, KeyInfo)
			If Not ki.LikeBtn Then
				CType(sender, Control).BackColor = BtnOnMouseHoverColor
			End If
		End If
	End Sub

	Private Sub Lab_MouseLeave(sender As Object, e As EventArgs)
		Dim ki = CType(CType(sender, Control).Tag, KeyInfo)
		If Not ki.LikeBtn Then
			CType(sender, Control).BackColor = BtnColor
		End If
	End Sub

	Private Overloads Sub OnResize() Handles MyBase.Resize
		SyncLock (_locker)
			If _keys IsNot Nothing Then
				Dim x = 0
				Dim y = 0
				Dim yStep = (Height - 1) / _keys.Count
				Dim lastRow = _keys.Last
				For Each keyRow In _keys
					If keyRow.Equals(lastRow) Then
						yStep = (Height - y - 1)
					End If

					Dim xSum = 0
					For Each key In keyRow
						If key Is Nothing Then
							xSum += 0.5
						Else
							xSum += key.WidthKoef
						End If
					Next
					Dim xStep = (Width - 1) / xSum

					Dim last = keyRow.Last
					If keyRow IsNot Nothing Then
						For Each key In keyRow
							If key IsNot Nothing Then
								If key.Equals(last) Then
									key.Control.Width = (Width - x - 1 - 1)
								Else
									key.Control.Width = xStep * key.WidthKoef - 1
								End If
								key.Control.Height = yStep - 1
								key.Control.Left = x + 1
								key.Control.Top = y + 1
								x += xStep * key.WidthKoef
							Else
								x += xStep * 0.5
							End If
						Next
					End If
					x = 0
					y += yStep
				Next
			End If
		End SyncLock
	End Sub

End Class


