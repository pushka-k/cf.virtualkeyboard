﻿Imports System.Windows.Forms

Public Class VirtKeysForm
	Public Sub New(focusControl As Control, x As Integer, y As Integer, Optional passMode As Boolean = False)
		InitializeComponent()
		WindowState = FormWindowState.Normal
		StartPosition = FormStartPosition.Manual
		TopMost = True
		TopLevel = True
		Top = y
		Left = x
		VirtKeysForForm1.PasswordMode = passMode
		Me.FocusControl = focusControl
	End Sub

	Public Property FocusControl As Control

	Private Overloads Sub OnShown()
		If FocusControl IsNot Nothing Then
			FocusControl.Focus()
		End If
	End Sub

	Public Property PassMode As Boolean
		Get
			Return VirtKeysForForm1.PasswordMode
		End Get
		Set(value As Boolean)
			VirtKeysForForm1.PasswordMode = value
		End Set
	End Property

	Private Sub OnKeyPressed(key As String) Handles VirtKeysForForm1.KeyPressed
		Try
			If key.Contains("Закр") Then
				Close()
			ElseIf key.Contains("Стереть") Then
				FocusControl.Focus()
				SendKeys.Send("{BACKSPACE}")
			Else
				If FocusControl IsNot Nothing Then
					FocusControl.Focus()
					SendKeys.Send(key)
					'Me.Focus()
				End If
			End If
		Catch ex As Exception
			'
		End Try
	End Sub

End Class