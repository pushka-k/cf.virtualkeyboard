﻿Imports System.Drawing

Public Class VirtKeys2
	Inherits VirtualKeyboardControl

	Public Sub New()
		InitializeComponent()

		Dim stdKeyWidth = 1

		Dim row1 = New List(Of KeyInfo)({
			New KeyInfo("1", "!", stdKeyWidth),
			New KeyInfo("2", "@", stdKeyWidth),
			New KeyInfo("3", "#", stdKeyWidth),
			New KeyInfo("4", "$", stdKeyWidth),
			New KeyInfo("5", "%", stdKeyWidth),
			New KeyInfo("6", "^", stdKeyWidth),
			New KeyInfo("7", "?", stdKeyWidth),
			New KeyInfo("8", "*", stdKeyWidth),
			New KeyInfo("9", "(", stdKeyWidth),
			New KeyInfo("0", ")", stdKeyWidth),
			New KeyInfo("Стереть", "", stdKeyWidth * 3)
		})

		Dim row2 = New List(Of KeyInfo)({
			New KeyInfo("~", "ё", stdKeyWidth, True),
			New KeyInfo("q", "й", stdKeyWidth, True),
			New KeyInfo("w", "ц", stdKeyWidth, True),
			New KeyInfo("e", "у", stdKeyWidth, True),
			New KeyInfo("r", "к", stdKeyWidth, True),
			New KeyInfo("t", "е", stdKeyWidth, True),
			New KeyInfo("y", "н", stdKeyWidth, True),
			New KeyInfo("u", "г", stdKeyWidth, True),
			New KeyInfo("i", "ш", stdKeyWidth, True),
			New KeyInfo("o", "щ", stdKeyWidth, True),
			New KeyInfo("p", "з", stdKeyWidth, True),
			New KeyInfo("[", "х", stdKeyWidth, True),
			New KeyInfo("]", "ъ", stdKeyWidth, True)
		})

		Dim row3 = New List(Of KeyInfo)({
			New KeyInfo("a", "ф", stdKeyWidth, True),
			New KeyInfo("s", "ы", stdKeyWidth, True),
			New KeyInfo("d", "в", stdKeyWidth, True),
			New KeyInfo("f", "а", stdKeyWidth, True),
			New KeyInfo("g", "п", stdKeyWidth, True),
			New KeyInfo("h", "р", stdKeyWidth, True),
			New KeyInfo("j", "о", stdKeyWidth, True),
			New KeyInfo("k", "л", stdKeyWidth, True),
			New KeyInfo("l", "д", stdKeyWidth, True),
			New KeyInfo(";", "ж", stdKeyWidth, True),
			New KeyInfo("'", "э", stdKeyWidth, True),
			New KeyInfo("Ввод", "", stdKeyWidth * 1.7)
		})

		Dim row4 = New List(Of KeyInfo)({
			New KeyInfo("Shift", "", stdKeyWidth * 2),
			New KeyInfo("z", "я", stdKeyWidth, True),
			New KeyInfo("x", "ч", stdKeyWidth, True),
			New KeyInfo("c", "с", stdKeyWidth, True),
			New KeyInfo("v", "м", stdKeyWidth, True),
			New KeyInfo("b", "и", stdKeyWidth, True),
			New KeyInfo("n", "т", stdKeyWidth, True),
			New KeyInfo("m", "ь", stdKeyWidth, True),
			New KeyInfo(",", "б", stdKeyWidth, True),
			New KeyInfo(".", "ю", stdKeyWidth, True),
			New KeyInfo("/", ".", stdKeyWidth, True),
			New KeyInfo("\", ",", stdKeyWidth, True)
		})

		Dim row5 = New List(Of KeyInfo)({
			New KeyInfo("Закрыть" + vbCrLf + "клавиатуру", "", stdKeyWidth * 2),
			New KeyInfo("Пробел", "", stdKeyWidth * 3.5),
			New KeyInfo("-", "_", stdKeyWidth * 0.7),
			New KeyInfo("=", "+", stdKeyWidth * 0.7),
			New KeyInfo("Del", "", stdKeyWidth),
			New KeyInfo("Рус", "Англ", stdKeyWidth * 1.2, False, True)
			})

		BtnColor = Color.Gainsboro
		SetKeys({row1, row2, row3, row4, row5})
	End Sub
End Class
