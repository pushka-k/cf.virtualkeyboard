﻿Imports System.Windows.Forms

Public Class KeyInfo

	Public Sub New(text As String, textShifted As String, widthKoef As Double, Optional otherLang As Boolean = False, Optional langBtn As Boolean = False)
		Me.Text = text
		Me.TextShifted = textShifted
		Me.WidthKoef = widthKoef
		Me.OtherLanguage = otherLang
		Me.LangBtn = langBtn
	End Sub

	Public Property Control As Control
	Public Property Text As String
	Public Property TextShifted As String
	Public Property WidthKoef As Double
	Public Property OtherLanguage As Boolean
	Public Property LangBtn

	Public Property LikeBtn As Boolean
End Class